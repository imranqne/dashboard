<?php if(!defined('__ACCESS__')) die("Access Denied ...");

class Users extends Emails{

	var $user_id; 
	var $user_first_name;
	var $user_last_name;
	var $user_full_name;
	var $user_email;
	var $user_password;
	var $user_added_date;
	var $user_register_ip;
	var $user_address;
	var $user_city; 
	var $user_state;
	var $user_country;
	var $user_parent_id;
	var $user_role_id;
	var $user_status;
	var $user_basic_config;
	var $user_verify;
	var $user_ac_verification_code;

	function __construct(){
		// set defaults ...
	}

	public function userLogin($post){
		$username		= 	addslashes($post['username']);
		$user_pin 		= 	addslashes($post['user_pin']);
		$error			=	"Error: ";
		
		if(!isset($username) || empty($username))
		{
			return $error . "Please Enter Username";
		}
		else
		if(!isset($user_pin) || empty($user_pin))
		{
			return $error . "Please Enter Your Pin Code";
		} 
		else 
		{
			$DB 	= 	new DB_connection();
			$select = 	"SELECT * FROM ". GM_EMPLOYEES ." WHERE username = '". $username ."' ";
			$conn	=	$DB->query($select);
			$rows	=	mysql_num_rows($conn);
			$DB->done();   // Close mySql DB Connection ...
			
			if($rows <= 0)
			{
				return $error . "Username does not exist.";
			} 
			else 
			{
				$fetch	=	mysql_fetch_object($conn);
				
				if($user_pin != $fetch->pin_password)
				{
					return $error . "Invalid Credentials.";
				} 
				else 
				{
					$_SESSION['employee_id'] 	= 	$fetch->employee_id;
					$_SESSION['username'] 		= 	$username;
					$_SESSION['account_type'] 	= 	$fetch->account_type;
					$_SESSION['saleman']		= 	$fetch->first_name . " " . $fetch->last_name;
					
					return 'success';
				}
			}
		}
	}
	
	function createLead()
	{
		extract($_POST);
		$DB 	= 	new DB_connection();
		$insert	=	"INSERT INTO `gm_form_entries` (`id`, `first_name`, `last_name`, `phone_num`, `email_address`, `type`, `year`, `make`, `model`, `stock_num`, `salesperson1`, `salesperson2`, `lead_source`, `notes`, `duebill`, `date`, `time`) VALUES
('', '" . addslashes($first_name) . "', '" . addslashes($last_name) . "', '" . addslashes($phone) . "', '" . addslashes($email) . "', '" . $type . "', '" . $year . "', '" . $make . "', '" . $model . "', '" . $stock . "', '" . $salesperson . "', '" . $salesperson2 . "', '" . $lead_source . "', '" . addslashes($notes) . "', '" . $duebill . "', '" . date('Y-d-m') . "', '" . date('H:i:s') . "')";
		$DB->query($insert);
		$DB->done(); 
		return true;
	}

	public function top20Sales($start, $limit)
	{
		$DB 	= 	new DB_connection();
		$select = 	"SELECT * FROM ". GM_SALES ." ORDER BY id DESC LIMIT " . $start . ", " . $limit;
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...
		
		$salesman 	= 	array();
		if($rows > 0)
		{
			$c			=	0;
			
			while($fetch = mysql_fetch_object($conn))
			{
				$salesman[$c]					=	new Users();
				$salesman[$c]->id				=	$fetch->id;
				$salesman[$c]->first_name		=	$fetch->first_name;
				$salesman[$c]->last_name		=	$fetch->last_name;
				$salesman[$c]->phone_num		=	$fetch->phone_num;
				$salesman[$c]->email_address	=	$fetch->email_address;
				$salesman[$c]->type				=	$fetch->type;
				$salesman[$c]->year				=	$fetch->year;
				$salesman[$c]->make				=	$fetch->make;
				$salesman[$c]->model			=	$fetch->model;
				$salesman[$c]->stock_num		=	$fetch->stock_num;
				$salesman[$c]->salesperson1		=	$fetch->salesperson1;
				$salesman[$c]->salesperson2		=	$fetch->salesperson2;
				$salesman[$c]->lead_source		=	$fetch->lead_source;
				$salesman[$c]->notes			=	$fetch->notes;
				$salesman[$c]->duebill			=	$fetch->duebill;
				$salesman[$c]->date				=	$fetch->date;
				$salesman[$c]->time				=	$fetch->time;
 				$c++;
			}
		}
		return $salesman;
	}
	
	public function salesByDateType($date, $type)
	{
		$DB 	= 	new DB_connection();
		$select = 	"SELECT * FROM ". GM_SALES ." WHERE date = '" . $date . "' AND type = '" . $type . "' ORDER BY id DESC";
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...
		
		$salesman 	= 	array();
		if($rows > 0)
		{
			$c			=	0;
			
			while($fetch = mysql_fetch_object($conn))
			{
				$salesman[$c]					=	new Users();
				$salesman[$c]->id				=	$fetch->id;
				$salesman[$c]->first_name		=	$fetch->first_name;
				$salesman[$c]->last_name		=	$fetch->last_name;
				$salesman[$c]->phone_num		=	$fetch->phone_num;
				$salesman[$c]->email_address	=	$fetch->email_address;
				$salesman[$c]->type				=	$fetch->type;
				$salesman[$c]->year				=	$fetch->year;
				$salesman[$c]->make				=	$fetch->make;
				$salesman[$c]->model			=	$fetch->model;
				$salesman[$c]->stock_num		=	$fetch->stock_num;
				$salesman[$c]->salesperson1		=	$fetch->salesperson1;
				$salesman[$c]->salesperson2		=	$fetch->salesperson2;
				$salesman[$c]->lead_source		=	$fetch->lead_source;
				$salesman[$c]->notes			=	$fetch->notes;
				$salesman[$c]->date				=	$fetch->date;
				$salesman[$c]->time				=	$fetch->time;
 				$c++;
			}
		}
		return $salesman;
	}
	
	public function totalSalesByEmp($emp)
	{
		$last	=	date('Y-m-') . date('t',strtotime('today'));
		$DB 	= 	new DB_connection();
		$select = 	"SELECT count(*) as sales FROM " . GM_SALES . " WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND salesperson1 = '" . $emp . "'";
		$conn	=	$DB->query($select);
		$fetch 	= 	mysql_fetch_object($conn);
		
		$this->sales	=	$fetch->sales;
		
		$select2 		= 	"SELECT count(*) as goal FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND duebill = 'y' AND salesperson1 = '" . $emp . "'";
		$conn2			=	$DB->query($select2);
		$fetch2 		= 	mysql_fetch_object($conn2);
		
		$this->goal		=	$fetch2->goal;
		$this->tracking	=	$this->sales - $this->goal;
		
		if($this->sales > 0)
		{
			$this->percent	=	round(($this->goal / $this->sales) * 100);	
		}
		else
		{
			$this->percent	=	0;
		}
		$DB->done();   // Close mySql DB Connection ...
	}
	
	public function empSalesByType($emp, $type)
	{
		$last	=	date('Y-m-') . date('t',strtotime('today'));
		$DB 	= 	new DB_connection();
		$select = 	"SELECT count(*) as sales FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND type = '" . $type . "' AND salesperson1 = '" . $emp . "'";
		$conn	=	$DB->query($select);
		$DB->done(); 
		$fetch 	= 	mysql_fetch_object($conn);
		
		return $this->saleType	=	$fetch->sales;
		  // Close mySql DB Connection ...
	}
	
	public function totalSales()
	{
		$last	=	date('Y-m-') . date('t',strtotime('today'));
		$DB 	= 	new DB_connection();
		$select = 	"SELECT count(*) as sales FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "'";
		$conn	=	$DB->query($select);
		$fetch 	= 	mysql_fetch_object($conn);
		
		$this->sales	=	$fetch->sales;
		
		$select2 		= 	"SELECT count(*) as goal FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND duebill = 'y'";
		$conn2			=	$DB->query($select2);
		$fetch2 		= 	mysql_fetch_object($conn2);
		
		$this->goal		=	$fetch2->goal;
		$this->tracking	=	$this->sales - $this->goal;
		$DB->done();   // Close mySql DB Connection ...
	}
	
	public function salesByType($type)
	{
		$last	=	date('Y-m-') . date('t',strtotime('today'));
		$DB 	= 	new DB_connection();
		$select = 	"SELECT count(*) as sales FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND type = '" . $type . "'";
		$conn	=	$DB->query($select);
		$fetch 	= 	mysql_fetch_object($conn);
		
		$this->sales	=	$fetch->sales;
		
		$select2 		= 	"SELECT count(*) as goal FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND duebill = 'y' AND type = '" . $type . "'";
		$conn2			=	$DB->query($select2);
		$fetch2 		= 	mysql_fetch_object($conn2);
		
		$this->goal		=	$fetch2->goal;
		$this->tracking	=	$this->sales - $this->goal;
		$DB->done();   // Close mySql DB Connection ...
	}
	
	public function salesByLeadSource($lead)
	{
		$last	=	date('Y-m-') . date('t',strtotime('today'));
		$DB 	= 	new DB_connection();
		$select = 	"SELECT count(*) as sales FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND lead_source = '" . $lead . "'";
		$conn	=	$DB->query($select);
		$fetch 	= 	mysql_fetch_object($conn);
		
		$this->sales	=	$fetch->sales;
		
		$select2 		= 	"SELECT count(*) as goal FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND duebill = 'y' AND lead_source = '" . $lead . "'";
		$conn2			=	$DB->query($select2);
		$fetch2 		= 	mysql_fetch_object($conn2);
		
		$this->goal		=	$fetch2->goal;
		$this->tracking	=	$this->sales - $this->goal;
		$DB->done();   // Close mySql DB Connection ...
	}
	
	public function ledgerDetails($id)
	{
		$DB 	= 	new DB_connection();
		$select = 	"SELECT * FROM ". GM_SALES ." WHERE id = " . $id;
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...

		if($rows > 0)
		{
			$fetch = mysql_fetch_object($conn);

			$this->id				=	$fetch->id;
			$this->first_name		=	$fetch->first_name;
			$this->last_name		=	$fetch->last_name;
			$this->phone_num		=	$fetch->phone_num;
			$this->email_address	=	$fetch->email_address;
			$this->type				=	$fetch->type;
			$this->year				=	$fetch->year;
			$this->make				=	$fetch->make;
			$this->model			=	$fetch->model;
			$this->stock_num		=	$fetch->stock_num;
			$this->salesperson1		=	$fetch->salesperson1;
			$this->salesperson2		=	$fetch->salesperson2;
			$this->lead_source		=	$fetch->lead_source;
			$this->notes			=	$fetch->notes;
			$this->duebill			=	$fetch->duebill;
			$this->date				=	$fetch->date;
			$this->time				=	$fetch->time;
		}
	}
	
	public function leadSalesLedger($lead)
	{
		$last	=	date('Y-m-') . date('t',strtotime('today'));
		$DB 	= 	new DB_connection();
		$select = 	"SELECT id, first_name, type, year, make, model, stock_num FROM ". GM_SALES ." WHERE date >= '" . date('Y-m-01') . "' AND date <= '" . $last . "' AND lead_source = '" . $lead . "'";
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();
		
		$salesman 	= 	array();
		if($rows > 0)
		{
			$c			=	0;
			
			while($fetch = mysql_fetch_object($conn))
			{
				$salesman[$c]					=	new Users();
				$salesman[$c]->id				=	$fetch->id;
				$salesman[$c]->first_name		=	$fetch->first_name;
				$salesman[$c]->type				=	$fetch->type;
				$salesman[$c]->year				=	$fetch->year;
				$salesman[$c]->make				=	$fetch->make;
				$salesman[$c]->model			=	$fetch->model;
				$salesman[$c]->stock_num		=	$fetch->stock_num;
				$c++;
			}
		}
		return $salesman;
	}

	public function searchLedger($salesperson, $duebill, $search)
	{
		$DB 	= 	new DB_connection();
		
		$searchKeyword	=	'';
		if($salesperson != '')
		{
			$searchKeyword	.=	" WHERE (salesperson1 = '" . $salesperson . "' || salesperson2 = '" . $salesperson . "')";
		}
		
		if($duebill != '')
		{
			if($searchKeyword == '')
			{
				$searchKeyword	.=	" WHERE duebill = '" . $duebill . "'";
			}
			else
			{
				$searchKeyword	.=	" AND duebill = '" . $duebill . "'";
			}
		}
		
		if($search != '')
		{
			if($searchKeyword == '')
			{
				$searchKeyword	.=	" WHERE make LIKE '%" . $search . "%'";
			}
			else
			{
				$searchKeyword	.=	" AND make LIKE '%" . $search . "%'";
			}
		}
		
		$select = 	"SELECT * FROM ". GM_SALES . $searchKeyword ." ORDER BY id DESC LIMIT 0,20";
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...
		
		$salesman 	= 	array();
		if($rows > 0)
		{
			$c			=	0;
			
			while($fetch = mysql_fetch_object($conn))
			{
				$salesman[$c]					=	new Users();
				$salesman[$c]->id				=	$fetch->id;
				$salesman[$c]->first_name		=	$fetch->first_name;
				$salesman[$c]->last_name		=	$fetch->last_name;
				$salesman[$c]->phone_num		=	$fetch->phone_num;
				$salesman[$c]->email_address	=	$fetch->email_address;
				$salesman[$c]->type				=	$fetch->type;
				$salesman[$c]->year				=	$fetch->year;
				$salesman[$c]->make				=	$fetch->make;
				$salesman[$c]->model			=	$fetch->model;
				$salesman[$c]->stock_num		=	$fetch->stock_num;
				$salesman[$c]->salesperson1		=	$fetch->salesperson1;
				$salesman[$c]->salesperson2		=	$fetch->salesperson2;
				$salesman[$c]->lead_source		=	$fetch->lead_source;
				$salesman[$c]->notes			=	$fetch->notes;
				$salesman[$c]->date				=	$fetch->date;
				$salesman[$c]->time				=	$fetch->time;
 				$c++;
			}
		}
		return $salesman;
	}

	public function salespersonDetails($id)
	{
		$DB 	= 	new DB_connection();
		$select = 	"SELECT * FROM ". GM_EMPLOYEES ." WHERE employee_id = " . $id;
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...

		if($rows > 0)
		{
			$fetch = mysql_fetch_object($conn);

			$this->employee_id		=	$fetch->employee_id;
			$this->first_name		=	$fetch->first_name;
			$this->last_name		=	$fetch->last_name;
			$this->avatar			=	$fetch->avatar;
			$this->username			=	$fetch->username;
			$this->pin_password		=	$fetch->pin_password;
			$this->email			=	$fetch->email;
			$this->phone			=	$fetch->phone;
			$this->type				=	$fetch->account_type;
		}
	}
		
	public function allSalesPerson()
	{
		$DB 	= 	new DB_connection();
		$select = 	"SELECT * FROM ". GM_EMPLOYEES ." WHERE account_type = 'sales' ORDER BY employee_id DESC";
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...
		
		$salesman 	= 	array();
		if($rows > 0)
		{
			$c			=	0;
			
			while($fetch = mysql_fetch_object($conn))
			{
				$salesman[$c]					=	new Users();
				$salesman[$c]->employee_id		=	$fetch->employee_id;
				$salesman[$c]->account_type		=	$fetch->account_type;
				$salesman[$c]->first_name		=	$fetch->first_name;
				$salesman[$c]->last_name		=	$fetch->last_name;
				$salesman[$c]->avatar			=	$fetch->avatar;
				$salesman[$c]->username			=	$fetch->username;
				$salesman[$c]->pin_password		=	$fetch->pin_password;
				$salesman[$c]->email			=	$fetch->email;
				$salesman[$c]->phone			=	$fetch->phone;
 				$c++;
			}
		}
		return $salesman;
	}
	
	public function salesPersonListing($start, $limit)
	{
		$DB 	= 	new DB_connection();
		$select = 	"SELECT * FROM ". GM_EMPLOYEES ." WHERE account_type = 'sales' ORDER BY employee_id DESC LIMIT " . $start . ", " . $limit;
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...
		
		$salesman 	= 	array();
		if($rows > 0)
		{
			$c			=	0;
			
			while($fetch = mysql_fetch_object($conn))
			{
				$salesman[$c]					=	new Users();
				$salesman[$c]->employee_id		=	$fetch->employee_id;
				$salesman[$c]->account_type		=	$fetch->account_type;
				$salesman[$c]->first_name		=	$fetch->first_name;
				$salesman[$c]->last_name		=	$fetch->last_name;
				$salesman[$c]->avatar			=	$fetch->avatar;
				$salesman[$c]->username			=	$fetch->username;
				$salesman[$c]->pin_password		=	$fetch->pin_password;
				$salesman[$c]->email			=	$fetch->email;
				$salesman[$c]->phone			=	$fetch->phone;
 				$c++;
			}
		}
		return $salesman;
	}
	
	public function allMake()
	{
		$DB 	= 	new DB_connection();
		$select = 	"SELECT * FROM ". GM_MAKE ." ORDER BY make_id ASC";
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...
		
		$makes 	= 	array();
		if($rows > 0)
		{
			$c			=	0;
			
			while($fetch = mysql_fetch_object($conn))
			{
				$makes[$c]				=	new Users();
				$makes[$c]->make_id		=	$fetch->make_id;
				$makes[$c]->make		=	$fetch->make;
 				$c++;
			}
		}
		return $makes;
	}
	
	public function allModelByMake($make)
	{
		$DB 	= 	new DB_connection();
		$select = 	"SELECT gmm.model_id, gmm.model FROM ". GM_MODEL ." gmm JOIN " . GM_MAKE . " gmk ON gmm.make_id = gmk.make_id WHERE gmk.make = '" . $make . "' ORDER BY model_id";
		$conn	=	$DB->query($select);
		$rows	=	mysql_num_rows($conn);
		$DB->done();   // Close mySql DB Connection ...
		
		$models 	= 	array();
		if($rows > 0)
		{
			$c			=	0;
			
			while($fetch = mysql_fetch_object($conn))
			{
				$models[$c]				=	new Users();
				$models[$c]->model_id	=	$fetch->model_id;
				$models[$c]->model		=	$fetch->model;
 				$c++;
			}
		}
		return $models;
	}

	public function updateSalesperson()
	{
		$DB 	= 	new DB_connection();
		extract($_POST);
		$update	=	"UPDATE `". GM_EMPLOYEES ."` SET `account_type` = '". $type . "', `first_name` = '". $first_name . "', `last_name` = '". $last_name . "', `username` = '". $username . "', `pin_password` = '". $passwrd . "', `email` = '". $email . "', `phone` = '". $phone . "' WHERE employee_id = " . $emp_id;;
		$DB->query($update);
		
		if(move_uploaded_file($_FILES['avatar']['tmp_name'], 'assets/img/employees/' . $emp_id . "_" . $_FILES['avatar']['name']))
		{
			$update2	=	"UPDATE `". GM_EMPLOYEES ."` SET avatar = '". $emp_id . "_" . $_FILES['avatar']['name'] ."' WHERE employee_id = ". $emp_id;
			$DB->query($update2);
		}
		
		$DB->done();   // Close mySql DB Connection ...
		return true;
	}
	
	public function addSalesperson()
	{
		$DB 	= 	new DB_connection();
		extract($_POST);
		$insert	=	"INSERT INTO `". GM_EMPLOYEES ."` (`employee_id`, `account_type`, `first_name`, `last_name`, `username`, `pin_password`, `email`, `phone`) VALUES 
('', '" . $type . "', '" . addslashes($first_name) . "', '" . addslashes($last_name) . "', '" . addslashes($username) . "', '" . addslashes($passwrd) . "', '" . addslashes($email) . "', '" . addslashes($phone) . "')";

		$DB->query($insert);
		$emp_id	=	mysql_insert_id();
		
		if(move_uploaded_file($_FILES['avatar']['tmp_name'], 'assets/img/employees/' . $emp_id . "_" . $_FILES['avatar']['name']))
		{
			$update	=	"UPDATE `". GM_EMPLOYEES ."` SET avatar = '". $emp_id . "_" . $_FILES['avatar']['name'] ."' WHERE employee_id = ". $emp_id;
			$DB->query($update);
		}
		$DB->done();   // Close mySql DB Connection ...
		return true;
	}
} // Class End ...
?>