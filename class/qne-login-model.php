<?php
class Login extends DB_connection 
{
	var $connection;

	public function Login()
	{
		$this->connection 	=  	new DB_connection();
		$this->user_id		=	0;
		$this->user_type	=	0;
		$this->username		=	"";
		$this->f_name		=	"";
		$this->l_name		=	"";
		$this->email		=	"";
		$this->status		=	0;
	}
	
	public function loginUser()
	{
		$DB			= 	new DB_connection();
		extract($_POST);
		$select 	= 	"SELECT qu.*, ut.type, ut.user_rights FROM `inv_qne_users` qu LEFT JOIN `inv_qne_user_type` ut ON qu.user_type = ut.type_id WHERE (qu.username = '" . mysql_real_escape_string($username) . "' || qu.email = '" . mysql_real_escape_string($username) . "') AND qu.status = 1";
		
		$conn		= 	$DB->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$fet	=	mysql_fetch_object($conn);
			
			if($fet->password == base64_encode($password))
			{
				$this->user_id 		= 	$fet->user_id;
				$this->user_type	=	$fet->user_type;
				$this->username		=	$fet->username;
				$this->f_name		=	$fet->f_name;
				$this->l_name		=	$fet->l_name;
				$this->email		=	$fet->email;
				$this->password		=	$fet->password;
				$this->status		=	$fet->status;
				$this->datetime		=	$fet->datetime;
				$this->type			=	$fet->type;
				$this->user_rights	=	$fet->user_rights;
				
				$_SESSION['sess_user_id']		=	$fet->user_id;
				$_SESSION['sess_user_type']		=	$fet->user_type;
				$_SESSION['sess_username']		=	$fet->username;
				$_SESSION['sess_f_name']		=	$fet->f_name;
				$_SESSION['sess_l_name']		=	$fet->l_name;
				$_SESSION['sess_email']			=	$fet->email;
				$_SESSION['sess_user_rights']	=	$fet->user_rights;
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}	
	
	public function logoutUser()
	{
		$_SESSION['sess_user_type']	=	"";
		$_SESSION['sess_username']	=	"";
		$_SESSION['sess_f_name']	=	"";
		$_SESSION['sess_l_name']	=	"";
		$_SESSION['sess_email']		=	"";
		unset($_SESSION);
	}
	
	public function registerUser()
	{
		$DB			= 	new DB_connection();
		extract($_POST);
		$select 	= 	"SELECT * FROM `inv_qne_users` WHERE `username` = '" . mysql_real_escape_string($username) . "' || `email` = '" . mysql_real_escape_string($email) . "'";
		$conn		= 	$DB->query($select);
		
		if(mysql_num_rows($conn) <= 0)
		{
			$insert	=	"INSERT INTO `inv_qne_users` (`user_id`, `user_type`, `username`, `f_name`, `l_name`, `email`, `password`, `status`, `datetime`) 
						VALUES ('', '0', '" . mysql_real_escape_string($username) . "', '" . mysql_real_escape_string($f_name) . "', '" . mysql_real_escape_string($l_name) . "', '" . mysql_real_escape_string($email) . "', '" . base64_encode(mysql_real_escape_string($password)) . "', '0', '" . date('Y-m-d H:i:s') . "')";
			$DB->query($insert);
			return 1;
		}
		else
		{
			$fetch	=	mysql_fetch_object($conn);
			
			if($fetch->username == mysql_real_escape_string($username))
			{
				return 0;
			}
			else
			if($fetch->email == mysql_real_escape_string($email))
			{
				return 2;
			}
			
		}
	}
	
	public function resetPassword($email)
	{
		$DB			= 	new DB_connection();
		extract($_POST);
		$select 	= 	"SELECT * FROM `inv_qne_users` WHERE `username` = '" . mysql_real_escape_string($username) . "' || `email` = '" . mysql_real_escape_string($email) . "'";
		$conn		= 	$DB->query($select);
		
		if(mysql_num_rows($conn) <= 0)
		{
			$fetch	=	mysql_fetch_object($conn);
			
			$emailTo	=	$fetch->email;
		
			$headers 	= 	"MIME-Version: 1.0\n";
			$headers   .= 	"Content-type: text/html; charset=iso-8859-1\n";
			$headers   .= 	"From: QuicknEasy Inventory <info@qne.com.pk>\n";
			$headers   .= 	"X-Mailer: PHP's mail() Function\n";
			
			$subject 	= 	"Password Retrieval for QuicknEasy Inventory";
			
			$body		=	'<table  border="0" cellpadding="20" cellspacing="0" width="100%">
                    <tr><td>
					<div> 
                          <br/><p style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left; padding-left: 10px;">
						 <br/><strong>Dear '.$fetch->f_name . ' ' . $fetch->l_name . ',</strong><br /></p>
						<p style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left; padding-left: 10px;">
Following are your login details:<br/></p>
                            <p style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left; padding-left: 10px;"><strong>Your Email Address is :</strong> '.$fetch->email . '<br/></p>

                            <p style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left; padding-left: 10px;"><strong>Your Password is :</strong> ' . base64_decode($fetch->password) . '<br/></p>
							
							<p style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left; padding-left: 10px;">If you are facing any problems, please feel free to contact our customer service support team.</p>
							
							   
                          </div></td>
                      </tr>
					</table>';

			if(@mail($to,$subject,$body,$headers))
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}
	
	function allUserType($type_id=0)
	{
		$DB			= 	new DB_connection();
		 $where		=	"";
		 
		if($user_id != 0)
		{
			 $where	=	" WHERE `type_id` = " . $type_id;
		}
		$select 	= 	"SELECT * FROM `inv_qne_user_type`" . $where;
		$conn		= 	$DB->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$userTypes 	= 	array();
			$c			=	0;
			while($fetch = mysql_fetch_object($conn))
			{	
				$userTypes[$c]				=	new Login();
				$userTypes[$c]->type_id		=	$fetch->type_id;
				$userTypes[$c]->type		=	$fetch->type;
				$userTypes[$c]->status		=	$fetch->status;
				$c++;
			}
			return $userTypes;
		}
	}	
	
	function checkUsername($username)
	{
		$DB			= 	new DB_connection();

		$select 	= 	"SELECT * FROM `inv_qne_users` WHERE `username` = '" . $username . "'";
		$conn		= 	$DB->query($select);
		
		if(mysql_num_rows($conn) <= 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function checkEmail($email)
	{
		$DB			= 	new DB_connection();

		$select 	= 	"SELECT * FROM `inv_qne_users` WHERE `email` = '" . $email . "'";
		$conn		= 	$DB->query($select);
		
		if(mysql_num_rows($conn) <= 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}	
	
	function allUsers($user_id=0)
	{
		$DB			= 	new DB_connection();
		 $where		=	"";
		 
		if($user_id != 0)
		{
			 $where	=	" WHERE `user_id` = " . $user_id;
		}
		$select 	= 	"SELECT * FROM `inv_qne_users`" . $where;
		$conn		= 	$DB->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$users 	= 	array();
			$c				=	0;
			while($fetch = mysql_fetch_object($conn))
			{	
				$users[$c]				=	new Login();
				$users[$c]->user_id		=	$fetch->user_id;
				$users[$c]->user_type	=	$fetch->user_type;
				$users[$c]->username	=	$fetch->username;
				$users[$c]->f_name		=	$fetch->f_name;
				$users[$c]->l_name		=	$fetch->l_name;
				$users[$c]->email		=	$fetch->email;
				$users[$c]->password	=	base64_decode($fetch->password);
				$users[$c]->status		=	$fetch->status;
				$users[$c]->datetime	=	$fetch->datetime;
				$c++;
			}
			return $users;
		}
	}	
	
	function addUser($post)
	{
		$DB				= 	new DB_connection();
		$username		=	mysql_real_escape_string($post['username']);
		$user_type		=	mysql_real_escape_string($post['user_type']);
		$f_name			=	mysql_real_escape_string($post['f_name']);
		$l_name			=	mysql_real_escape_string($post['l_name']);
		$email			=	mysql_real_escape_string($post['email']);
		$password		=	mysql_real_escape_string($post['password']);
		$re_password	=	mysql_real_escape_string($post['re_password']);
		$status			=	mysql_real_escape_string($post['status']);
		
		$select 	= 	"INSERT INTO `inv_qne_users`(`user_id`, `user_type`, `f_name`, `l_name`, `email`, `password`, `status`, `datetime`) VALUES('', '" . $user_type . "', '" . $f_name . "', '" . $l_name . "', '" . $email . "', '" . $password . "', '" . $status . "', '" . date('Y-m-d H:i:s') . "')";
		if($DB->query($select))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function editUser($post)
	{
		$DB				= 	new DB_connection();
		$user_id		=	mysql_real_escape_string($post['user_id']);
		$username		=	mysql_real_escape_string($post['username']);
		$user_type		=	mysql_real_escape_string($post['user_type']);
		$f_name			=	mysql_real_escape_string($post['f_name']);
		$l_name			=	mysql_real_escape_string($post['l_name']);
		$email			=	mysql_real_escape_string($post['email']);
		$password		=	base64_encode(mysql_real_escape_string($post['password']));
		$status			=	mysql_real_escape_string($post['status']);
		
		$select 	= 	"UPDATE `inv_qne_users` SET `user_type` = '" . $user_type . "', `f_name` = '" . $f_name . "', `l_name` = '" . $l_name . "', `email` = '" . $email . "', `password` = '" . $password . "', `status` = '" . $status . "' WHERE `user_id` = '" . $user_id . "'";
		if($DB->query($select))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/*public function allCategory($parent_id=0)
	{
		$DB			= 	new DB_connection();
		$select 	= 	"SELECT * FROM `inv_qne_category` WHERE parent_id = " . $parent_id;
		$conn		= 	$DB->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$categories 	= 	array();
			$c				=	0;
			while($fetch = mysql_fetch_object($conn))
			{	
				$categories[$c]					=	new General();
				$categories[$c]->category_id	=	$fetch->category_id;
				$categories[$c]->parent_id		=	$fetch->parent_id;
				$categories[$c]->title			=	$fetch->title;
				$categories[$c]->url_title		=	$fetch->url_title;
				$categories[$c]->des			=	$fetch->des;
				$categories[$c]->sort_order		=	$fetch->sort_order;
				$categories[$c]->seotitle		=	$fetch->seotitle;
				$categories[$c]->metatags		=	$fetch->metatags;
				$categories[$c]->metadesc		=	$fetch->metadesc;
				$categories[$c]->status			=	$fetch->status;
				$categories[$c]->datetime		=	$fetch->datetime;
				$c++;
			}
			return $categories;
		}
	}*/
}
?>