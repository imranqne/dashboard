<?php
class General extends DB_connection 
{
	var $connection;
	var $designation_id;

	public function General()
	{
		$this->connection =  new DB_connection();
		$this->designation_id	=	0;
		$this->contact_id		=	0;
		$this->brand_id			=	0;
		$this->distributor_id	=	0;
	}
    
    public function assignRidersBarcode($riderId,$Barcode)
	{
		$updateRiderAssign 	= 	"UPDATE z_orders set delivery_rider_id = '".$riderId."' where serial_number = '".$Barcode."' ";
		$conn		= 	$this->connection->query($updateRiderAssign);
		
	}

	public function allRiders($rider_id=0)
	{
		if($rider_id != 0 && is_numeric($rider_id))
		{
			$Where = " AND `id` = " . $rider_id;
		}
		$select 	= 	"SELECT * FROM `delivery_riders` WHERE `status` = 1" . $Where;
		$conn		= 	$this->connection->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$ridersArr 	= 	array();
			$c				=	0;
			while($fetch = mysql_fetch_object($conn))
			{	
				$ridersArr[$c]->id			= $fetch->id;
				$ridersArr[$c]->Name		= $fetch->Name;
				$ridersArr[$c]->first_name	= $fetch->first_name;
				$ridersArr[$c]->last_name	= $fetch->last_name;
				$ridersArr[$c]->avatar		= $fetch->avatar;
				$ridersArr[$c]->mobile		= $fetch->mobile;
				$ridersArr[$c]->email		= $fetch->email;
				$ridersArr[$c]->password	= $fetch->password;
				$ridersArr[$c]->status		= $fetch->status;
				$ridersArr[$c]->last_login	= $fetch->last_login;
				$ridersArr[$c]->created		= $fetch->created;
				$c++;
			}
			return $ridersArr;
		}
	}
    
    public function fetchSideMenu($parentId=0)
	{
		
		$select 	= 	"SELECT * FROM `dashboard_pages` where parentId = '".$parentId."' ";
		$conn		= 	$this->connection->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$MenuArr 	= 	array();
			$c				=	0;
			while($fetch = mysql_fetch_object($conn))
			{	
				$MenuArr[$c]->id		= $fetch->id;
				$MenuArr[$c]->pageName	= $fetch->pageName;
				$MenuArr[$c]->pageUrl	= $fetch->pageUrl;
                $MenuArr[$c]->parentId	= $fetch->parentId;
				$MenuArr[$c]->icon		= $fetch->icon;
				$c++;
			}
			return $MenuArr;
		}
	}
	
	public function orderStatus($status_id=0)
	{
		if($status_id != 0 && is_numeric($status_id))
		{
			$Where = " AND `status_id` = " . $status_id;
		}
		$select = "SELECT * FROM `qne_order_status` WHERE `status` = '1'" . $Where;
		$conn 	= $this->connection->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$statusArr 	= array();
			$c			= 0;
			while($fetch = mysql_fetch_object($conn))
			{	
				$statusArr[$c]->status_id	= $fetch->status_id;
				$statusArr[$c]->order_status= $fetch->order_status;
				$statusArr[$c]->status		= $fetch->status;
				$statusArr[$c]->created		= $fetch->created;
				$c++;
			}
			return $statusArr;
		}
	}
	
	public function timeSlots()
	{
		$time 	= date('H');
		$select = "SELECT * FROM `qne_timeslots` WHERE " . $time . " BETWEEN `StartTime` AND `EndTime` ORDER BY `status` DESC";
		$conn	= $this->connection->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$timeSlotArr = array();
			$c			 = 0;
			while($fetch = mysql_fetch_object($conn))
			{	
				$timeSlotArr[$c]->timeslot_id	= $fetch->timeslot_id;
				$timeSlotArr[$c]->time_slot		= $fetch->time_slot;
				$timeSlotArr[$c]->timing		= $fetch->timing;
				$timeSlotArr[$c]->day			= $fetch->day;
				$timeSlotArr[$c]->status		= $fetch->status;
				$timeSlotArr[$c]->StartTime		= $fetch->StartTime;
				$timeSlotArr[$c]->EndTime		= $fetch->EndTime;
				$c++;
			}
			return $timeSlotArr;
		}
	}
	
	function getPOVersion($PONumber)
	{
		$sql 	=  "SELECT version FROM `inv_qne_purchase_order` WHERE `po_number` = '" . $PONumber . "' ORDER BY po_number DESC LIMIT 1";
		$conn	=	mysql_query($sql);
		
		if(mysql_num_rows($conn) > 0)
		{
			$fet	=	mysql_fetch_object($conn);
			return $version	=	(int)$fet->version + 1;
		}
		else
		{
			return 0;
		}
	}
	
	function companyCount()
	{
		$sql 	=  "SELECT count(company_id) as totalCompany FROM `inv_qne_company` WHERE `status` = '1' AND `is_delete` = 'n'";
		$conn	=	mysql_query($sql);
		
		if(mysql_num_rows($conn) > 0)
		{
			$fet = mysql_fetch_object($conn);
			return $company	= $fet->totalCompany;
		}
		else
		{
			return 0;
		}
	}
	
	function brandCount()
	{
		$sql 	=  "SELECT count(id) as totalBrand FROM `brand` WHERE `status` = '1'";
		$conn	=	mysql_query($sql);
		
		if(mysql_num_rows($conn) > 0)
		{
			$fet = mysql_fetch_object($conn);
			return $brands = $fet->totalBrand;
		}
		else
		{
			return 0;
		}
	}
	
	function productCount()
	{
		$sql 	=  "SELECT count(product_id) as totalProduct FROM `inv_qne_products` WHERE `status` = '1'";
		$conn	=	mysql_query($sql);
		
		if(mysql_num_rows($conn) > 0)
		{
			$fet = mysql_fetch_object($conn);
			return $products = $fet->totalProduct;
		}
		else
		{
			return 0;
		}
	}
	
	function productSKUCount()
	{
		$sql 	=  "SELECT count(sku_id) as totalSKU FROM `inv_qne_product_sku` WHERE `status` = '1'";
		$conn	=	mysql_query($sql);
		
		if(mysql_num_rows($conn) > 0)
		{
			$fet = mysql_fetch_object($conn);
			return $products = $fet->totalSKU;
		}
		else
		{
			return 0;
		}
	}
}
?>