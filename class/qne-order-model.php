<?php
//Imran Edit
class orderModel extends DB_connection 
{
	var $connection;
	
	public function orderModel()
	{
		$this->connection 		=  new DB_connection();
		$this->designation_id	=	0;
		$this->contact_id		=	0;
		$this->brand_id			=	0;
		$this->distributor_id	=	0;
	}

	public function allOrders($orderStatus=0, $start=0, $limit=100)
	{
		$Where = '';
		if($orderStatus != 0)
		{
			$Where 	.= " AND `ord_status` = " . $orderStatus;
		}
		
		if($start == 0 && $limit == 0)
		{
			$select = "SELECT count(*) as totalRows FROM `z_orders` WHERE `deleted` = 0" . $Where;// . " ORDER BY `order_id` DESC";
			$conn	= $this->connection->query($select);
			
			$fetch  = mysql_fetch_object($conn);
			
			return $fetch->totalRows;
		}
		else
		{
			$select = "SELECT * FROM `z_orders` WHERE `deleted` = 0 " . $Where . " ORDER BY `order_id` DESC LIMIT " . $start . ", " . $limit;
			$conn	= $this->connection->query($select);
			
			if(mysql_num_rows($conn) > 0)
			{
				$orderDetails 	= 	array();
				$c				=	0;
				while($fetch = mysql_fetch_object($conn))
				{	
					$orderDetails[$c]->order_id				=	$fetch->order_id;
					$orderDetails[$c]->serial_number		=	$fetch->serial_number;
					$orderDetails[$c]->user_id				=	$fetch->user_id;
					$orderDetails[$c]->user_type			=	$fetch->user_type;
					$orderDetails[$c]->delivery_rider_id	=	$fetch->delivery_rider_id;
					$orderDetails[$c]->order_total			=	$fetch->order_total;
					$orderDetails[$c]->sub_total			=	$fetch->sub_total;
					$orderDetails[$c]->advance_payment		=	$fetch->advance_payment;
					$orderDetails[$c]->ord_cost_shipping	=	$fetch->ord_cost_shipping;
					$orderDetails[$c]->reschedule_shipping	=	$fetch->reschedule_shipping;
					$orderDetails[$c]->mpos_charges			=	$fetch->mpos_charges;
					$orderDetails[$c]->discount_price		=	$fetch->discount_price;
					$orderDetails[$c]->discount_percent		=	$fetch->discount_percent;
					$orderDetails[$c]->discount_code		=	$fetch->discount_code;
					$orderDetails[$c]->ord_tax_cost			=	$fetch->ord_tax_cost;
					$orderDetails[$c]->billing_fname		=	$fetch->billing_fname;
					$orderDetails[$c]->billing_lname		=	$fetch->billing_lname;
					$orderDetails[$c]->billing_phone		=	$fetch->billing_phone;
					$orderDetails[$c]->billing_cnic			=	$fetch->billing_cnic;
					$orderDetails[$c]->user_email			=	$fetch->user_email;
					$orderDetails[$c]->billing_address		=	$fetch->billing_address;
					$orderDetails[$c]->delivery_fname		=	$fetch->delivery_fname;
					$orderDetails[$c]->delivery_lname		=	$fetch->delivery_lname;
					$orderDetails[$c]->delivery_gender		=	$fetch->delivery_gender;
					$orderDetails[$c]->delivery_address		=	$fetch->delivery_address;
					$orderDetails[$c]->delivery_address2	=	$fetch->delivery_address2;
					$orderDetails[$c]->delivery_phone		=	$fetch->delivery_phone;
					$orderDetails[$c]->delivery_mobile		=	$fetch->delivery_mobile;
					$orderDetails[$c]->delivery_city		=	$fetch->delivery_city;
					$orderDetails[$c]->delivery_state		=	$fetch->delivery_state;
					$orderDetails[$c]->delivery_country		=	$fetch->delivery_country;
					$orderDetails[$c]->delivery_zipcode		=	$fetch->delivery_zipcode;
					$orderDetails[$c]->delivery_cnic		=	$fetch->delivery_cnic;
					$orderDetails[$c]->special_note			=	$fetch->special_note;
					$orderDetails[$c]->admin_notes			=	$fetch->admin_notes;
					$orderDetails[$c]->order_placed_date	=	$fetch->order_placed_date;
					$orderDetails[$c]->date_				=	$fetch->date_;
					$orderDetails[$c]->ord_status			=	$fetch->ord_status;
					$orderDetails[$c]->ord_payment_method	=	$fetch->ord_payment_method;
					$orderDetails[$c]->shipping_araeId		=	$fetch->shipping_araeId;
					$orderDetails[$c]->shipping_araeName	=	$fetch->shipping_araeName;
					
					$orderDetails[$c]->shipping_araeTimeslotId		=	$fetch->shipping_araeTimeslotId;
					$orderDetails[$c]->shipping_araeTimeslotTitle	=	$fetch->shipping_araeTimeslotTitle;
					$orderDetails[$c]->ord_ip_address				=	$fetch->ord_ip_address;
					$orderDetails[$c]->inventry_less_status			=	$fetch->inventry_less_status;
					$orderDetails[$c]->userRedeem_add_status		=	$fetch->userRedeem_add_status;
					$orderDetails[$c]->used_redeemPoint				=	$fetch->used_redeemPoint;
					$orderDetails[$c]->used_redeemPoint_price		=	$fetch->used_redeemPoint_price;
					$orderDetails[$c]->referralUserId				=	$fetch->referralUserId;
					$orderDetails[$c]->deleted						=	$fetch->deleted;
					$orderDetails[$c]->admin_note					=	$fetch->admin_note;
					$orderDetails[$c]->resource						=	$fetch->resource;
					$c++;
				}
				return $orderDetails;
			}
		}	
	}
	
	public function orderDetail($orderID)
	{
		$select = "SELECT * FROM `z_orders` WHERE `order_id` = " . $orderID;
		$conn	= $this->connection->query($select);
		
		if(mysql_num_rows($conn) > 0)
		{
			$fetch = mysql_fetch_object($conn);
			
			$this->order_id				=	$fetch->order_id;
			$this->serial_number		=	$fetch->serial_number;
			$this->user_id				=	$fetch->user_id;
			$this->user_type			=	$fetch->user_type;
			$this->delivery_rider_id	=	$fetch->delivery_rider_id;
			$this->order_total			=	$fetch->order_total;
			$this->sub_total			=	$fetch->sub_total;
			$this->advance_payment		=	$fetch->advance_payment;
			$this->ord_cost_shipping	=	$fetch->ord_cost_shipping;
			$this->reschedule_shipping	=	$fetch->reschedule_shipping;
			$this->mpos_charges			=	$fetch->mpos_charges;
			$this->discount_price		=	$fetch->discount_price;
			$this->discount_percent		=	$fetch->discount_percent;
			$this->discount_code		=	$fetch->discount_code;
			$this->ord_tax_cost			=	$fetch->ord_tax_cost;
			$this->billing_fname		=	$fetch->billing_fname;
			$this->billing_lname		=	$fetch->billing_lname;
			$this->billing_phone		=	$fetch->billing_phone;
			$this->billing_cnic			=	$fetch->billing_cnic;
			$this->user_email			=	$fetch->user_email;
			$this->billing_address		=	$fetch->billing_address;
			$this->delivery_fname		=	$fetch->delivery_fname;
			$this->delivery_lname		=	$fetch->delivery_lname;
			$this->delivery_gender		=	$fetch->delivery_gender;
			$this->delivery_address		=	$fetch->delivery_address;
			$this->delivery_address2	=	$fetch->delivery_address2;
			$this->delivery_phone		=	$fetch->delivery_phone;
			$this->delivery_mobile		=	$fetch->delivery_mobile;
			$this->delivery_city		=	$fetch->delivery_city;
			$this->delivery_state		=	$fetch->delivery_state;
			$this->delivery_country		=	$fetch->delivery_country;
			$this->delivery_zipcode		=	$fetch->delivery_zipcode;
			$this->delivery_cnic		=	$fetch->delivery_cnic;
			$this->special_note			=	$fetch->special_note;
			$this->admin_notes			=	$fetch->admin_notes;
			$this->order_placed_date	=	$fetch->order_placed_date;
			$this->date_				=	$fetch->date_;
			$this->ord_status			=	$fetch->ord_status;
			$this->ord_payment_method	=	$fetch->ord_payment_method;
			$this->shipping_araeId		=	$fetch->shipping_araeId;
			$this->shipping_araeName	=	$fetch->shipping_araeName;
			
			$this->shipping_araeTimeslotId		=	$fetch->shipping_araeTimeslotId;
			$this->shipping_araeTimeslotTitle	=	$fetch->shipping_araeTimeslotTitle;
			$this->ord_ip_address				=	$fetch->ord_ip_address;
			$this->inventry_less_status			=	$fetch->inventry_less_status;
			$this->userRedeem_add_status		=	$fetch->userRedeem_add_status;
			$this->used_redeemPoint				=	$fetch->used_redeemPoint;
			$this->used_redeemPoint_price		=	$fetch->used_redeemPoint_price;
			$this->referralUserId				=	$fetch->referralUserId;
			$this->deleted						=	$fetch->deleted;
			$this->admin_note					=	$fetch->admin_note;
			$this->resource						=	$fetch->resource;
		}
	}

    public function changeStatusBarcode($barcode,$status)
	{
        $Update = "Update `z_orders` SET `ord_status` = '" . $status . "' WHERE `serial_number` = '" . $barcode . "'";
        $this->connection->query($Update) or die('1');

        $SelectOrdId =  "SELECT order_id from z_orders where serial_number = '".$barcode."'";
        $conn	= $this->connection->query($SelectOrdId);
        $fetch = mysql_fetch_object($conn);
        
        //Update Order History
        $ChangeStatusDate 	= date('D d M Y h:i:s A');
        $InsertHistory = "INSERT INTO `order_history` SET order_id = '" . $fetch->order_id . "', order_status = '" . $status . " (by " . $_SESSION['username'] . ")', status_time = '" . $ChangeStatusDate . "'";
        $conn	= $this->connection->query($InsertHistory);
	}
	
	public function assignRider($orderID, $riderID)
	{
        $update = "UPDATE `z_orders` SET `delivery_rider_id` = '" . $riderID . "' WHERE `order_id` = '" . $orderID . "'";
        $this->connection->query($update) or die('1');
		
		$selectQry 	= "SELECT id, Name FROM `delivery_riders` WHERE `id` = '" . $riderID . "'";
        $connQry	= $this->connection->query($selectQry);
		$riderName  = "RIDER";
		
		if(mysql_num_rows($connQry) > 0)
		{
			$fetch 		= mysql_fetch_object($connQry);
			$riderName 	= mysql_real_escape_string($fetch->Name);
		}

        $statusDateTime	= date('D d M Y h:i:s A');
		$orderStatus	= str_replace("SESSION_USERNAME", $riderName, RIDER_STATUS);
		$insertHistory 	= "INSERT INTO `order_history` (`order_id`, `order_status`, `status_time`, `note`) VALUES('" . $orderID . "', '" . $orderStatus . "', '" . $statusDateTime . "', '')";
        $conn	= $this->connection->query($insertHistory);
		return $riderName;
	}
	
	public function changeTimeSlot($orderID, $slotID)
	{
		$selectQry 	= "SELECT timeslot_id, time_slot FROM `qne_timeslots` WHERE `timeslot_id` = '" . $slotID . "'";
        $connQry	= $this->connection->query($selectQry);
		$slotName  	= "TIME SLOT";
		
		if(mysql_num_rows($connQry) > 0)
		{
			$fetch 		= mysql_fetch_object($connQry);
			$slotName 	= mysql_real_escape_string($fetch->time_slot);
		}
		
        $update = "UPDATE `z_orders` SET `shipping_araeTimeslotId` = '" . $slotID . "', `shipping_araeTimeslotTitle` = '" . $slotName . "' WHERE `order_id` = '" . $orderID . "'";
        $this->connection->query($update) or die('1');
		
		$statusDateTime	= date('D d M Y h:i:s A');
		$orderStatus	= str_replace("SESSION_USERNAME", $_SESSION['username'], TIMESLOT_STATUS);
		$insertHistory 	= "INSERT INTO `order_history` (`order_id`, `order_status`, `status_time`, `note`) VALUES('" . $orderID . "', '" . $orderStatus . "', '" . $statusDateTime . "', '')";
        $conn	= $this->connection->query($insertHistory);
		return $slotName;
	}
	
	public function orderItemsDetail($orderID)
	{
		$orderDetailQry		= "SELECT * FROM `z_orders_detail` WHERE `ord_id` = '" . $orderID . "'";	
		$orderDetailConn	= $this->connection->query($orderDetailQry);
		
		if(mysql_num_rows($orderDetailConn) > 0)
		{
			$orderDetails 	= 	array();
			$c				=	0;
					
			while($row2 = mysql_fetch_array($orderDetailConn))
			{
				$query_product_name = $this->connection->query("SELECT * FROM product where productid='".$row2['prd_id']."'");
				$get_product_name 	= mysql_fetch_array($query_product_name);
				$sizes			   	= '';
				$AttrCodes 			= '';
				
				if($row2['prod_type'] == 'mixNmatch')
				{
					$MixOfferimg = $this->connection->query("SELECT * FROM `z_orders_offer_details` WHERE `orderDetail_id`=".$row2['order_detail_id']." AND orderId='".$_GET['id']."' ORDER BY `id` DESC");

					while($MixOfferimg_rs = mysql_fetch_assoc($MixOfferimg))
					{
						$ProdAttimags = mysql_fetch_assoc($this->connection->query('SELECT pImg.productname, pImg.thumbnail1, AttImg.thumbnail_img, AttImg.attribute_title, AttImg.attribute_code FROM `product_attributes` AttImg  INNER JOIN product pImg ON AttImg.productid=pImg.productid WHERE `id`="'.$MixOfferimg_rs['stock_sku_id'].'" '));
						$AttrCodes   .= $ProdAttimags['productname'] . " - " . $ProdAttimags['attribute_title'] . " (" . $ProdAttimags['attribute_code'].")<br />";  
					}
				}
				else
				if($row2['prod_type'] == 'bundle')
				{
					$offer_qryAttrib =  $this->connection->query("SELECT p.productname, productcode, sum(offer_sku_qty) as offer_sku_qty, without_desc_price, final_price, pa.attribute_code,pa.attribute_title FROM `product_offers_details` pod JOIN `product` p ON pod.prod_id = p.productid JOIN `product_attributes` pa ON pod.sku_id = pa.id WHERE `offer_id` = " . $row2['prd_id'] . " GROUP BY sku_id");
					while($offer_detail_rs = mysql_fetch_assoc($offer_qryAttrib))
					{
						$AttrCodes .= $offer_detail_rs['productname'] . " (" . $offer_detail_rs['offer_sku_qty'] . ' x ' . $offer_detail_rs['attribute_title'] . " - " . $offer_detail_rs['attribute_code'] . ")<br />";
					}
				}
				else
				{ 
					if($row2['size_id'])
					{
						$ProductAttribute 	= mysql_fetch_assoc($this->connection->query("SELECT * FROM `product_attributes` WHERE id=".$row2['size_id']));
					}
					$AttrCodes = $ProductAttribute['attribute_title'];
				}
				
				$orderDetails[$c]->productTitle		= $get_product_name['productname'];
				$orderDetails[$c]->productAttribute	= $AttrCodes;
				$orderDetails[$c]->productRate		= $row2['prd_price'];
				$orderDetails[$c]->productQty		= $row2['prd_qty'];
				$orderDetails[$c]->productTotal		= $row2['prd_total_price'];
				$c++;
			}
			return $orderDetails;	
		}
	}	
}
?>