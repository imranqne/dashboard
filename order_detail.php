<?php
	include('class/class.php'); 
	
	/*$productModel	= 	new Product();
	$purchaseModel	= 	new Purchase();*/
	$generalModel	= 	new General();
	$orderModel		= 	new orderModel();
	
	$orderID		=	$_GET['id'];
	
	$orderModel->orderDetail($orderID);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Order Detail - Invoice # <?php echo $orderModel->serial_number; ?> | Admin Panel - QuicknEasy</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo PLUGINS; ?>loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo PLUGINS; ?>visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="<?php echo JS; ?>core/app.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>pages/dashboard.js"></script>

	<script type="text/javascript" src="<?php echo PLUGINS; ?>ui/ripple.min.js"></script>
	<!-- /theme JS files -->
	<link type="image/x-icon" href="<?php echo IMAGES; ?>favicon.ico" rel="icon">
</head>

<body>
	<!-- Main navbar -->
	<?php include('includes/header.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('includes/sidemenu.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header hidden-print">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Invoices</span> - Templates</h4>

							<ul class="breadcrumb position-right">
								<li><a href="index.html">Home</a></li>
								<li><a href="invoice_template.html">Invoices</a></li>
								<li class="active">Templates</li>
							</ul>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Invoice template -->
					<div class="panel panel-white" id="printInvoice">
						<div class="panel-heading hidden-print">
							<h6 class="panel-title">Static invoice</h6>
							<div class="heading-elements">
								<!--<button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-file-check position-left"></i> Save</button>-->
								<button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
		                	</div>
						</div>

						<div class="panel-body no-padding-bottom">
							<div class="row">
								<table class="col-sm-12">
									<tr>
										<td class="col-sm-6"><img src="assets/images/invoice_logo.png" class="content-group mt-10" alt="" style="width: 120px;"></td>
										<td class="col-sm-3">&nbsp;</td>
										<td class="col-sm-3 text-right"><h5 class="text-uppercase text-semibold">Invoice # <?php echo $orderModel->serial_number; ?></h5>
										<ul class="list-condensed list-unstyled">
											<li>Customer: <span class="text-semibold"><?php echo $orderModel->billing_fname . " " . $orderModel->billing_lname; ?></span></li>
											<li>Date: <span class="text-semibold"><?php echo date('d M, Y', strtotime($orderModel->date_)); ?></span></li>
										</ul></td>
									
									</tr>
								</table>	
								<!--<div class="col-sm-6 content-group">
									<img src="assets/images/invoice_logo.png" class="content-group mt-10" alt="" style="width: 120px;">
		 						</div>

								<div class="col-sm-6 content-group">
									<div class="invoice-details">
										<h5 class="text-uppercase text-semibold">Invoice # <?php echo $orderModel->serial_number; ?></h5>
										<ul class="list-condensed list-unstyled">
											<li>Customer: <span class="text-semibold"><?php echo $orderModel->billing_fname . " " . $orderModel->billing_lname; ?></span></li>
											<li>Date: <span class="text-semibold"><?php echo date('d M, Y', strtotime($orderModel->date_)); ?></span></li>
										</ul>
									</div>
								</div>-->
							</div>
						</div>

						<div class="table-responsive">
						    <table class="table table-lg">
						        <thead>
						            <tr>
						                <th class="col-sm-4">PRODUCT</th>
										<th class="col-sm-3">SKU</th>
						                <th class="col-sm-2">RATE</th>
						                <th class="col-sm-1">QTY</th>
						                <th class="col-sm-2">TOTAL</th>
						            </tr>
						        </thead>
						        <tbody>
<?php 								$orderDetails = $orderModel->orderItemsDetail($orderID);	
									if(sizeof($orderDetails) > 0)
									{
										$itemCount = 0;
										foreach($orderDetails as $detail)
										{
?>											<tr>
												<td><?php echo $detail->productTitle; ?></td>
												<td><?php echo $detail->productAttribute; ?></td>
												<td><?php echo number_format($detail->productRate, 2, '.', ','); ?></td>
												<td><?php echo $detail->productQty; ?></td>
												<td><span class="text-semibold"><?php echo number_format($detail->productTotal, 2, '.', ','); ?></span></td>
											</tr>
<?php										$itemCount++;
										}
									}		
?>								</tbody>
						    </table>
						</div>

						<div class="panel-body">
							<div class="row invoice-payment">
								<div class="col-sm-12">
									<?php /*<div class="content-group">
										<?php /*<h6>Authorized person</h6>
										<div class="mb-15 mt-15">
											<img src="assets/images/signature.png" class="display-block" style="width: 150px;" alt="">
										</div>

										<ul class="list-condensed list-unstyled text-muted">
											<li>Eugene Kopyov</li>
											<li>2269 Elba Lane</li>
											<li>Paris, France</li>
											<li>888-555-2311</li>
										</ul>
									</div>
								</div>

								<div class="col-sm-5">*/ ?>
									<div class="content-group" style="width:60%; float:right">
										<h5>INVOICE DETAILS</h5>
										<div class="table-responsive no-border">
											<table class="table">
												<tbody>
<?php 												$expressDelivery = 0;
													if($orderModel->shipping_araeTimeslotId == 'ED')
													{
														$expressDelivery = EXPRESS_DELIVERY_CHARGES;
													}
?>												
													<tr>
														<th>PAYMENT METHOD:</th>
														<td class="text-right"><?php echo $orderModel->ord_payment_method; ?></td>
													</tr>
													<tr>
														<th>TOTAL ITEMS:</th>
														<td class="text-right"><?php echo $itemCount; ?></td>
													</tr>
					
													<tr>
														<th>SUB-TOTAL:</th>
														<td class="text-right"><?php echo CURRENCY . " " . number_format($orderModel->sub_total + $orderModel->discount_price + $orderModel->advance_payment - $expressDelivery, 2); ?></td>
													</tr>
													
<?php 												if($orderModel->shipping_araeTimeslotId == 'ED')
													{
?>														<tr>
															<th>EXPRESS DELIVERY CHARGES:</th>
															<td class="text-right"><?php echo CURRENCY . " " . number_format(EXPRESS_DELIVERY_CHARGES, 2); ?></td>
														</tr>
<?php	 											}
?>						
													<tr>
														<th>SHIPPING CHARGES:</th>
														<td class="text-right"><?php echo CURRENCY . " " . number_format($orderModel->ord_cost_shipping, 2); ?></td>
													</tr>

<?php 												if($orderModel->reschedule_shipping != 0)
													{
?>														<tr>
															<th>RE-SCHEDULE CHARGES:</th>
															<td class="text-right"><?php echo CURRENCY . " " . number_format($orderModel->reschedule_shipping, 2); ?></td>
														</tr>
<?php												}														

													if($orderModel->mpos_charges != 0)
													{
?>														<tr>
															<th>mPOS CHARGES:</th>
															<td class="text-right"><?php echo CURRENCY . " " . number_format($orderModel->mpos_charges, 2); ?></td>
														</tr>
<?php 												}
?>
													<tr>
														<th>DISCOUNT PRICE:</th>
														<td class="text-right"><?php echo CURRENCY . " " . number_format($orderModel->discount_price, 2); ?></td>
													</tr>
						
<?php 												if($orderModel->used_redeemPoint_price != 0)
													{
?>														<tr>
															<th>REDEEM PRICE:</th>
															<td class="text-right"><?php echo CURRENCY . " " . number_format($orderModel->used_redeemPoint_price, 2); ?></td>
														</tr>
<?php 												}
						
													if($orderModel->advance_payment != 0)
													{
?>														<tr>
															<th>ADVANCE PAYMENT:</th>
															<td class="text-right"><?php echo CURRENCY . " " . number_format($orderModel->advance_payment, 2); ?></td>
														</tr>	
<?php 												}													
?>													<tr>
														<th>TOTAL AMOUNT PAYABLE:</th>
														<td class="text-right"><h5 class="text-semibold"><?php echo CURRENCY . " " . number_format($orderModel->order_total + $orderModel->reschedule_shipping + $orderModel->ord_cost_shipping - $orderModel->used_redeemPoint_price, 2); ?></h5></td>
													</tr>
												</tbody>
											</table>
										</div>

										<div class="text-right hidden-print">
											<button type="button" class="btn btn-primary btn-labeled" onclick="printDiv('printInvoice');"><b><i class="icon-printer"></i></b> Print Invoice</button>
										</div>
									</div>
								</div>
							</div>

							<?php /*<h6>Other information</h6>
							<p class="text-muted">Thank you for using Limitless. This invoice can be paid via PayPal, Bank transfer, Skrill or Payoneer. Payment is due within 30 days from the date of delivery. Late payment is possible, but with with a fee of 10% per month. Company registered in England and Wales #6893003, registered office: 3 Goodman Street, London E1 8BF, United Kingdom. Phone number: 888-555-2311</p>*/ ?>
						</div>
					</div>
					<!-- /invoice template -->

					<!-- Footer -->
					<?php include('includes/footer.php'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<script>
        function printDiv(divName) 
		{
			var printContents = document.getElementById(divName).innerHTML;    
			var originalContents = document.body.innerHTML;      
			document.body.innerHTML = printContents;     
			window.print();     
			document.body.innerHTML = originalContents;
        }
    </script>
</body>
</html>
