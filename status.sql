CREATE TABLE `qne_order_status` (
  `status_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_status` varchar(15) DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

insert  into `qne_order_status`(`status_id`,`order_status`,`status`,`created`) values (1,'Pending','1','2017-05-30 10:50:15'),(2,'Confirm','1','2017-05-30 10:50:15'),(3,'In-Process','1','2017-05-30 10:50:15'),(4,'Dispatched','1','2017-05-30 10:50:15'),(5,'Delivered','1','2017-05-30 10:50:15'),(7,'Cancel','1','2017-05-30 10:50:15'),(9,'Closed','1','2017-05-30 10:50:15'),(10,'Rejected','1','2017-05-30 10:50:15'),(8,'Re-Schedule','1','2017-05-30 10:50:15');