<?php
class Config
         {
                var $_hostname;
                var $_username;
                var $_password;
                var $_database;
                var $_table_users;

               function Config()
                {
					
                        $this->_hostname        = 	HOSTDB;
                        $this->_username        = 	USERNAME;
                        $this->_password        = 	PASSWORD;
                        $this->_database        = 	DATABASE;
                        $this->_table_users		= 	array();  // the name of a table

                }

                function getHostname()                           {return $this->_hostname;}
                function getUsername()                           {return $this->_username;}
                function getPassword()                           {return $this->_password;}
                function getDatabase()                           {return $this->_database;}
                function getStudentTable()                       {return $this->_table_users[0];}
                
           }

         class DB_connection extends Config
         {
                var $_connection;
                var $_result;
				
               function DB_connection()
                {
						$this->Config();
						$this->_connection = mysql_connect($this->_hostname, $this->_username, $this->_password)
                                      or die("Unable to initialize connection to mysql database: ".mysql_error());
						mysql_select_db($this->_database) or die(mysql_error());
                }

              

                /*function query($query)
                {
                        $this->_result = mysql_db_query($this->_database, $query, $this->_connection)
                                  or die("Unable to query database: ".mysql_error());
                        return $this->_result;
                }*/
                
				function query($query)
                {
						$this->_result = mysql_query($query,$this->_connection)
                                  or die("Unable to query database: ".mysql_error());
                        return $this->_result;
                }

                function done()
                {
//                        mysql_free_result($this->_result);
                          mysql_close($this->_connection);
                }
                
         }

$connection = new DB_connection();

?>