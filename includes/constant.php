<?php
	define('SERVER', 'http://' . $_SERVER['HTTP_HOST'] . "/dashboard/");
	define('SERVERs', 'https://www.qne.com.pk/');

	define("HOSTDB"	, "localhost");
	define("DATABASE"	, "qauvtadvnk");
	define("USERNAME"	, "root");
	define("PASSWORD"	, "");
	
	/*define("HOSTDB"	, "qnecompk.fatcowmysql.com");
	define("DATABASE"	, "quickneasy");
	define("USERNAME"	, "qne_user");
	define("PASSWORD"	, "QnE@12345");*/

	define('CATEGORY', SERVER . 'category/');
	define('COMPANY', SERVER . 'company/');
	define('OUT', SERVER . 'out/');
	define('BLOG', SERVER . 'blog/');
	define('ABOUT_US', SERVER . 'about-us/');
	define('CURRENCY', 'Rs');
	define('EXPRESS_DELIVERY_CHARGES', 99);
	
	define('CONFIRM_STATUS', 'Order has been Confirmed (by SESSION_USERNAME)');
	define('INPROCESS_STATUS', 'Order is being processed (by SESSION_USERNAME)');
	define('RIDER_STATUS', 'Order is assign to (SESSION_USERNAME)');
	define('DISPATCHED_STATUS', 'Order has been dispatched (by SESSION_USERNAME)');
	define('DELIVERED_STATUS', 'Order has been Delivered (by SESSION_USERNAME)');
	define('CLOSE_STATUS', 'Order is Closed (by SESSION_USERNAME)');
	define('CANCEL_STATUS', 'Order has been cancelled (by SESSION_USERNAME)');
	define('RESCHEDULE_STATUS', 'Order has been Reschedule (by SESSION_USERNAME)');
	define('REJECTED_STATUS', 'Order has been Rejected (by SESSION_USERNAME)');
	define('TIMESLOT_STATUS', 'Order Timeslot changed (by SESSION_USERNAME)');
	
	define('ASSETS', SERVER . 'assets/');
	define('CSS', ASSETS . 'css/');
	define('JS', ASSETS . 'js/');
	define('PLUGINS', JS . 'plugins/');
	define('BOOTSTRAP', PLUGINS . 'bootstrap/');
	define('SCRIPTS', ASSETS . 'scripts/');
	define('POPUP', SERVER . 'popup_assets/');
	define('IMAGES', ASSETS . 'images/');
	define('PRODUCT_IMAGES', IMAGES . 'product_img/');
	define('ICONS', IMAGES . 'icons/');
	define('STORE_LOGO', ASSETS . 'images/company/logo/');
	define('SKU_IMAGES', PRODUCT_IMAGES . 'sku_img/');
	define('NO_IMAGE' , ASSETS . 'images/no_image.jpg');
	define('STORE_IMAGES', SERVER . 'images/stores/');
	define('STORE_SNAPSHOT', SERVER . 'images/stores/stores_snapshot/');

	define('SITE-NAME', 'QuicknEasy');
	define("DEFAULT_CURRENCY", "Rs");

	define('FEATURED_DATA_COUNT', '50');

	define('FACEBOOK', 'https://www.facebook.com/qnepk');
	define('LINKEDIN', 'https://www.linkedin.com/company/qne---online-grocery-store');
	define('TWITTER', 'https://twitter.com/qnepk');
	define('PINTEREST', 'https://www.pinterest.com/QnE_PK');
?>