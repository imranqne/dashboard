<?php
	include('class/class.php'); 
	
	/*$productModel	= 	new Product();
	$purchaseModel	= 	new Purchase();*/
	$generalModel	= 	new General();
	$orderModel		= 	new orderModel();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Manage Orders | Admin Panel - QuicknEasy</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo PLUGINS; ?>loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo PLUGINS; ?>visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="<?php echo JS; ?>core/app.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>pages/dashboard.js"></script>

	<script type="text/javascript" src="<?php echo PLUGINS; ?>ui/ripple.min.js"></script>
	<!-- /theme JS files -->
	<link type="image/x-icon" href="<?php echo IMAGES; ?>favicon.ico" rel="icon">
</head>

<body>

	<!-- Main navbar -->
	<?php include('includes/header.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('includes/sidemenu.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Dashboard</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">
							<!-- Quick stats boxes -->
							<div class="row">
								<div class="col-lg-4">

									<!-- Members online -->
									<div class="panel bg-teal-400">
										<div class="panel-body">
											<div class="heading-elements">
												<span class="heading-text badge bg-teal-800">+53,6%</span>
											</div>

											<h3 class="no-margin">3,450</h3>
											Members online
											<div class="text-muted text-size-small">489 avg</div>
										</div>

										<div class="container-fluid">
											<div id="members-online"></div>
										</div>
									</div>
									<!-- /members online -->

								</div>

								<div class="col-lg-4">

									<!-- Current server load -->
									<div class="panel bg-pink-400">
										<div class="panel-body">
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li class="dropdown">
							                			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li><a href="#"><i class="icon-sync"></i> Update data</a></li>
															<li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
															<li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
															<li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
														</ul>
							                		</li>
							                	</ul>
											</div>

											<h3 class="no-margin">49.4%</h3>
											Current server load
											<div class="text-muted text-size-small">34.6% avg</div>
										</div>

										<div id="server-load"></div>
									</div>
									<!-- /current server load -->

								</div>

								<div class="col-lg-4">

									<!-- Today's revenue -->
									<div class="panel bg-blue-400">
										<div class="panel-body">
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li><a data-action="reload"></a></li>
							                	</ul>
						                	</div>

											<h3 class="no-margin">$18,390</h3>
											Today's revenue
											<div class="text-muted text-size-small">$37,578 avg</div>
										</div>

										<div id="today-revenue"></div>
									</div>
									<!-- /today's revenue -->

								</div>
							</div>
							<!-- /quick stats boxes -->
							
							<!-- Marketing campaigns -->
<?php 						if(isset($_GET['status']) && is_numeric($_GET['status']))
							{
								$orderStatus = $_GET['status'];
								$statusQry   = "status=" . $orderStatus;
							}
							else
							{
								$orderStatus = 0;
								$statusQry   = "";
							}			
							
							$allOrders = $orderModel->allOrders($orderStatus, 0, 0);

							if(isset($_GET['page']))
							{
								$page  	= 	$_GET['page'];
								$limit 	= 	1;
							}	
							else
							{
								$page  	= 	1;
								$limit 	= 	1;
							}
			
							if($page>=1)
							{
								$page	=	($page - 1)*$limit;
							}				
				
							$tbl_name		=	"";		
							$adjacents 		= 	3;
							
							//$qString 		=	"?1=1";
							if(isset($_GET['status']) && is_numeric($_GET['status']))
							{
								$qString   = "?status=" . $orderStatus;
							}
							else
							{
								$qString 	= "?1=1";
							}
							$total_pages 	= 	$allOrders;
							$targetpage 	= 	"index.php" . $qString;
			
							$limit 	= 	100; 
							
							//$page 	= 	$_GET['page'];
			
							if($page) 
								$start = ($page - 1) * $limit; 			
							else
								$start = 0;							
							
							$allOrders = $orderModel->allOrders($orderStatus, $start, $limit);
							
							include('pagination.php');	
?>							
							<div class="panel panel-flat">
								<div class="table-responsive">
									<table class="table text-nowrap">
										<thead>
											<tr>
												<th class="col-md-3">ORDER DETAILS</th>
												<th class="col-md-2">ASSIGN RIDER</th>
												<th class="col-md-2">CHANGE STATUS</th>
												<th class="col-md-2">ORDER TIMESLOT</th>
												<th class="col-md-2">STATUS</th>
												<th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
											</tr>
										</thead>
										<tbody>
<?php										if(sizeof($allOrders) > 0)
											{
												$cnt = 0;	
												foreach($allOrders as $orderDetail)
												{
													switch($orderDetail->ord_status)
													{
														case 1:
															$iconImage = "pending.jpg";
															$label     = "bg-primary-400";
															$labelText = "Pending";
															$labelAplha= "P";
														break;
														
														case 2:
															$iconImage = "confirm.jpg";
															$label     = "bg-indigo-400";
															$labelText = "Confirm";
															$labelAplha= "CF";
														break;
														
														case 3:
															$iconImage = "inprocess.jpg";
															$label     = "bg-teal-400";
															$labelText = "In-Process";
															$labelAplha= "I";
														break;
														
														case 4:
															$iconImage = "dispatched.jpg";
															$label     = "bg-warning-400";
															$labelText = "Dispatched";
															$labelAplha= "DP";
														break;
														
														case 5:
															$iconImage = "delivered.jpg";
															$label     = "bg-pink-400";
															$labelText = "Delivered";
															$labelAplha= "DD";
														break;
														
														case 7:
															$iconImage = "cancel.jpg";
															$label     = "bg-danger";
															$labelText = "Cancel";
															$labelAplha= "CN";
														break;
														
														case 9:
															$iconImage = "closed.jpg";
															$label     = "bg-success-400";
															$labelText = "Closed";
															$labelAplha= "CL";
														break;
														
														case 10:
															$iconImage = "closed.jpg";
															$label     = "bg-grey-400";
															$labelText = "Rejected";
															$labelAplha= "R";
														break;

														default:
															$iconImage = "pending.jpg";
															$label     = "bg-blue";
															$labelText = "Pending";
															$labelAplha= "P";
														break;														
													}
													$dateTime 	 = date('d M Y h:i:s A',strtotime($orderDetail->order_placed_date));
													$orderAmount = $orderDetail->order_total + $orderDetail->reschedule_shipping + $orderDetail->ord_cost_shipping - $orderDetail->used_redeemPoint_price;
?>													<tr>
														<td>
															<div class="media-left media-middle">
																<!--<a href="#"><img src="<?php echo ICONS . $iconImage; ?>" class="img-circle img-xs" alt=""></a>-->
																<a href="#" class="btn <?php echo $label; ?> btn-rounded btn-icon btn-xs legitRipple"><span class="letter-icon"><?php echo $labelAplha; ?></span></a>
															</div>
															<div class="media-left">
																
																<div class=""><a href="#" class="text-default text-semibold"><?php echo $orderDetail->billing_fname . " " . $orderDetail->billing_lname;?></a></div>
																<div class="text-muted text-size-small">
																	<!--<span class="status-mark border-blue position-left"></span>-->
																	<strong>Invoice #: </strong><?php echo $orderDetail->serial_number; ?>
																</div>
																<div class="text-muted text-size-small"><strong>Order Date: </strong><?php echo $dateTime; ?></div>
																<div class="text-muted text-size-small"><strong>Shipping Area: </strong><?php echo substr($orderDetail->shipping_araeName,0,50); ?></div>
																<div class="text-muted text-size-small"><strong>Time-Slot: </strong><?php echo $orderDetail->shipping_araeTimeslotTitle; ?></div>
																<div class="text-muted text-size-small"><strong>Order Total: </strong><?php if($orderAmount <= 0){ echo number_format($orderAmount,2) . " (FOC)"; } else { echo CURRENCY . " " . number_format($orderAmount,2); } ?></div>
																<div class="text-muted text-size-small"><strong>Payment Method: </strong><?php echo $orderDetail->ord_payment_method; ?></div>
															</div>
														</td>
														<td>
															<div class="input-group">
																<div class="input-group-btn">
<?php																$allRiders = $generalModel->allRiders(0);
																	if(sizeof($allRiders) > 0)
																	{
																		$riderList = '';
																		$riderName = "RIDER";
																		foreach($allRiders as $rider)
																		{
																			$active = "";
																			if($rider->id == $orderDetail->delivery_rider_id)
																			{
																				$riderName = strtoupper($rider->Name);
																				$active    = 'class="active"';
																			}
																			$riderList .= '<li ' . $active . '><a href="javascript:assignRider(' . $orderDetail->order_id . ',' . $rider->id . ');">' . strtoupper($rider->Name) . '</a></li>';
																		}
																	}		
?>																	<button type="button" style="width:133px;" id="riderBtn_<?php echo $orderDetail->order_id; ?>" class="btn btn-default dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true"><?php echo $riderName; ?> <span class="caret"></span></button>
																	<ul class="dropdown-menu">
																		<?php echo $riderList; ?>
																	</ul>
																</div>
															</div>
														</td>
														<td>
															<div class="input-group">
																<div class="input-group-btn">
<?php																$orderStatus = $generalModel->orderStatus(0);
																	if(sizeof($orderStatus) > 0)
																	{
																		$statusList = '';
																		$statusName = "STATUS";
																		foreach($orderStatus as $status)
																		{
																			$active = "";
																			if($status->status_id == $orderDetail->ord_status)
																			{
																				$statusName = strtoupper($status->order_status);
																				$active    = 'class="active"';
																			}
																			$statusList .= '<li ' . $active . '><a href="javascript:assignStatus(' . $orderDetail->order_id . ',' . $status->status_id . ');">' . strtoupper($status->order_status) . '</a></li>';
																		}
																	}		
?>																	<button type="button" style="width:133px;" id="statusBtn_<?php echo $orderDetail->order_id; ?>" class="btn btn-default dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true"><?php echo $statusName; ?> <span class="caret"></span></button>
																	<ul class="dropdown-menu">
																		<?php echo $statusList; ?>
																	</ul>
																</div>
															</div>
														</td>
														<td>
															<div class="input-group">
																<div class="input-group-btn">
<?php																$timeSlots = $generalModel->timeSlots();
																	if(sizeof($timeSlots) > 0)
																	{
																		$slotList = '';
																		$slotName = "TIME SLOT";
																		foreach($timeSlots as $slot)
																		{
																			$active = "";
																			if($slot->timeslot_id == $orderDetail->shipping_araeTimeslotId)
																			{
																				$slotName = strtoupper($slot->time_slot);
																				$active     = 'class="active"';
																			}
																			$slotList .= '<li ' . $active . '><a href="javascript:changeTimeSlot(' . $orderDetail->order_id . ',' . $slot->timeslot_id . ');">' . strtoupper($slot->time_slot) . '</a></li>';
																		}
																	}		
?>																	<button type="button" style="width:133px;" id="timeSlotBtn_<?php echo $orderDetail->order_id; ?>" class="btn btn-default dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="true"><?php echo $slotName; ?> <span class="caret"></span></button>
																	<ul class="dropdown-menu">
																		<?php echo $slotList; ?>
																	</ul>
																</div>
															</div>
														</td>
														<td><span class="label <?php echo $label; ?>"><?php echo $labelText; ?></span></td>
														<td class="text-center">
															<ul class="icons-list">
																<li class="dropdown">
																	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
																	<ul class="dropdown-menu dropdown-menu-right">
																		<li><a href="order_detail.php?id=<?php echo $orderDetail->order_id; ?>"><i class="icon-file-stats"></i> Order Detail</a></li>
																		<li><a href="order_history.php?id=<?php echo $orderDetail->order_id; ?>"><i class="icon-list-unordered"></i> Order History</a></li>
																		<!--<li><a target="_blank" href="invoice_print.php?id=<?php echo $orderDetail->order_id; ?>"><i class="icon-printer"></i> Order Print</a></li>
																		<li class="divider"></li>-->
																		<li><a target="_blank" href="pos_print.php?id=<?php echo $orderDetail->order_id; ?>"><i class="icon-printer4"></i> POS Print</a></li>
																	</ul>
																</li>
															</ul>
														</td>
													</tr>
<?php											}
											}				
?>											
											
											<tr>
											<td align="right" colspan="6">
												<ul class="pagination pagination-separated pagination-rounded"><?php echo $pagination; ?></ul>
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- /marketing campaigns -->
						</div>
					</div>
					<!-- /dashboard content -->

					<!-- Footer -->
					<?php include('includes/footer.php'); ?>
					<!-- /footer -->
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
	</div>
	<script>
		function assignRider(orderID, riderID){
			var dataSource = "work=assignRider&orderID=" + orderID + "&riderID=" + riderID;
			$.ajax({
				type: "GET",
				url: "ajax.php",
				data: dataSource,
				success: function(rthtml){
					$('#riderBtn_' + orderID).html(rthtml + ' <span class="caret"></span>');
				}
			});
		}
	</script>
	
	<script>
		function changeTimeSlot(orderID, timeSlotID){
			var dataSource = "work=changeTimeSlot&orderID=" + orderID + "&timeSlotID=" + timeSlotID;
			$.ajax({
				type: "GET",
				url: "ajax.php",
				data: dataSource,
				success: function(rthtml){
					$('#timeSlotBtn_' + orderID).html(rthtml + ' <span class="caret"></span>');
				}
			});
		}
	</script>
	<!-- /page container -->
</body>
</html>