<?php

	include('class/class.php'); 
	
	$generalModel	= 	new General();
	/*$productModel	= 	new Product();
	$purchaseModel	= 	new Purchase();*/
	
	$orderModel		= 	new orderModel();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Manage Orders | Admin Panel - QuicknEasy</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS; ?>colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo PLUGINS; ?>loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo PLUGINS; ?>visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo PLUGINS; ?>pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="<?php echo JS; ?>core/app.js"></script>
	<script type="text/javascript" src="<?php echo JS; ?>pages/dashboard.js"></script>

	<script type="text/javascript" src="<?php echo PLUGINS; ?>ui/ripple.min.js"></script>
	<!-- /theme JS files -->
	<link type="image/x-icon" href="<?php echo IMAGES; ?>favicon.ico" rel="icon">
</head>

<body>

	<!-- Main navbar -->
	<?php include('includes/header.php'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include('includes/sidemenu.php'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Dashboard</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->

<script>
    function dispatch()
    {
        var barcoding = document.getElementById('invoice_bc').value;
        var riders = document.getElementById('riders').value;
        if(barcoding.length == 10)
            {
                //alert(barcode);
                order_dispatch(barcoding,riders);
            }
    }
    
    function order_dispatch(barcode,riderid){
       var linkdataStatus = "riderid=" + riderid + "&barcode=" + barcode + "&status=4&work=barcode_dispatch";
				$.ajax({
					type: "GET",
					url: "ajax.php",
					data: linkdataStatus,
					success: function(rthtml){
					//alert(rthtml);
					$('#invoice_bc').val('');
                    $("#suc").fadeIn("slow");
                    $("#suc").fadeOut(3000);
                    }
				});
    }
</script>
				<!-- Content area -->
				<div class="content">
					<!-- Dashboard content -->
					<div class="row">
				
                        
                        	<div class="panel panel-flat">
						<div class="panel-heading">
							<h6 class="panel-title">Assign Riders & Dispatch Orders</h6>
							<form class="form-horizontal" action="#">
									<div class="form-group">
			                        	<label class="control-label col-lg-2">Select Rider</label>
			                        	<div class="col-lg-10">
				                            <select name="riders" id="riders" class="form-control">
				                             <?php 
                                              $RidersList = $generalModel->allRiders();
                                                foreach($RidersList as $Riders)
                                                {
                                                    ?>
                                                <option value="<?php echo $Riders->id; ?>"><?php echo $Riders->Name; ?></option>
                                                <?php }
                                                ?>
				                            </select>
			                            </div>
			                        </div>
                                
                                 <div class="form-group">
										<label class="control-label col-lg-2">Barcode #</label>
										<div class="col-lg-10">
											<input autofocus name="invoice_bc" id="invoice_bc" onkeyup="dispatch()" type="text" class="form-control">
										</div>
									</div>
                                <span id="suc" style=" display:none;padding: 5px;background: green;color: white;">Successful</span>
                                
								</form>
							
						</div>
                                
                               
						

					</div>
					</div>
					<!-- /dashboard content -->

					<!-- Footer -->
					<?php include('includes/footer.php'); ?>
					<!-- /footer -->
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
	</div>
	<!-- /page container -->
</body>
</html>